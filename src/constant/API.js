export const BASE_URL = "http://localhost:8081/api/v1";

export const GET_PRODUCTS = "/product";
export const GET_CATEGORIES = "/category";

export const LOGIN = "/user/login";