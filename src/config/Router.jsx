import { Route, Routes } from "react-router-dom";
import Home from "../pages/home";
import Contact from "../pages/contact";
import About from "../pages/about";
import Checkout from '../pages/checkout';
import Login from '../pages/login';
import ProtectedRoute from "../components/protected";

const RouterPage = (
    <Routes>
        <Route path="/" element={<Login />} />
        <Route path="login" element={<Login />} />
        <Route path="home" element={<ProtectedRoute Component={Home} Role={"Admin"} />} />
        <Route path="contact" element={<ProtectedRoute Component={Contact} />} />
        <Route path="about" element={<About />} />
        <Route path="checkout" element={<ProtectedRoute Component={Checkout} />} />

    </Routes>
);
export default RouterPage;