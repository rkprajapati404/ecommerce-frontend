import React, { useContext, useEffect, useState } from 'react';
import { Layout, Input, Button, Avatar, Badge, Select } from 'antd';
import { UserOutlined, ShoppingCartOutlined, LogoutOutlined } from '@ant-design/icons';
import styles from './AppHeader.module.css';
import Logo from '../../assets/logo.png';
import AppContext from 'antd/es/app/context';
import { getCategories } from '../../services/productService';
import { useNavigate } from 'react-router-dom';
import { JWT_TOKEN } from '../../constant/AppConst';


const { Header } = Layout;
const { Search } = Input;
const { Option } = Select;

const HeaderMenu = () => {

  const contextData = useContext(AppContext);
  const [categories, setCategories] = useState([]);
  let navigate = useNavigate();


  console.log(contextData);

  const onSearch = (value) => {
    contextData.setSearchText(value);
  }

  async function fetchCategories() {
    try {
      const response = await getCategories();
      let result = response?.data;
      if (result?.success) {
        setCategories(result?.categories);
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    fetchCategories();
  }, []);

  const handleCategory = (input) => {
    contextData.setCategory(input)
  }

  const handleCart = () => {
    if (contextData.cart.length > 0) {
      navigate("/checkout");
    }
  }

  const handleLogin = () => {
    navigate("/login");
  }

  const handleLogout = () => {

    localStorage.removeItem(JWT_TOKEN);

    navigate("/login");
  }

  return (
    <div className={styles.appHeaderContainer}>
      <Header className={styles.appHeader}>

        <div className={styles.logo}>
          <img src={Logo} alt="Logo" style={{ marginTop: 25, marginRight: 10 }} />
        </div>

        <div>

          <Select value={contextData.category} style={{ width: 200 }} onChange={handleCategory}>
            <Option key={"all"}>All Category</Option>
            {
              categories.map((item) => {
                return (<Option key={item.name}>{item.name}</Option>)
              })
            }
          </Select>

        </div>

        <div className={styles.searchBar}>
          <Search placeholder="Search..." onSearch={onSearch} enterButton />
        </div>

        <div className={styles.loginSection}>

          {
            localStorage.getItem(JWT_TOKEN) ? null : <Button style={{ margin: 10 }} onClick={handleLogin} type="primary">Login</Button>
          }


          <Badge count={contextData.cart.length} className={styles.cartIcon} >
            <ShoppingCartOutlined className={contextData.cart.length === 0 ? styles.disabled : ''} disabled={!contextData.cart.length > 0} style={{ fontSize: '24px', cursor: 'pointer' }} onClick={handleCart} />
          </Badge>

          <Avatar style={{ marginLeft: 16, marginRight: 10 }} icon={<UserOutlined />} />

          {
            localStorage.getItem(JWT_TOKEN) ? <LogoutOutlined style={{ fontSize: '24px', cursor: 'pointer' }} onClick={handleLogout} /> : null
          }


        </div>

      </Header>

    </div>
  )
}

export default HeaderMenu;