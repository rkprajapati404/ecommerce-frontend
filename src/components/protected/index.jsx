import React, { useEffect } from 'react';
import { JWT_TOKEN } from '../../constant/AppConst';
import { useNavigate } from 'react-router-dom';

const ProtectedRoute = ({ Component, Role }) => {
    let token = localStorage.getItem(JWT_TOKEN);
    const navigate = useNavigate();
    useEffect(() => {
        if (!token) {
            navigate("/login");
        }

    }, [Component]);

    return (
        <Component />
    );
};

export default ProtectedRoute;
